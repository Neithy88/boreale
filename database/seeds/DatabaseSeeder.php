<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'firstname' => 'admin',
            'email' => 'bailly.antonin2@gmail.com',
            'validated' => 1,
            'link' => 'i5kmc7jd8b5d2gzyq5wciep2q7wxbps8',
            'account_type' => 'admin',
            'password' => bcrypt('secret'),
        ]);
        $id = DB::getPdo()->lastInsertId();

        DB::table('admins')->insert([
            'name' => 'admin',
            'firstname' => 'admin',
            'email' => 'bailly.antonin2@gmail.com',
            'password' => bcrypt('secret'),
            'id_user' => $id,
        ]);

        DB::table('users')->insert([
            'name' => 'patient',
            'firstname' => 'patient',
            'email' => 'patient@online.fr',
            'validated' => 1,
            'link' => '2snk0xl47qs711zmb82un0z1nwbege4q',
            'account_type' => 'patient',
            'password' => bcrypt('secret'),
        ]);
        $id = DB::getPdo()->lastInsertId();

        DB::table('patients')->insert([
            'name' => 'patient',
            'firstname' => 'patient',
            'email' => 'patient@online.fr',
            'address' => '30 rue du midi',
            'zip' => '58000',
            'city' => 'Nevers',
            'latitude' => 0,
            'longitude' => 0,
            'id_user' => $id,
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'name' => 'practitioner',
            'firstname' => 'practitioner',
            'email' => 'practitioner@online.fr',
            'validated' => 1,
            'link' => '2si5kmc7jd8b5d2gzyq5wciep2q7wxbp',
            'account_type' => 'practitioner',
            'password' => bcrypt('secret'),
        ]);
        $id = DB::getPdo()->lastInsertId();

        DB::table('practitioners')->insert([
            'name' => 'practitioner',
            'firstname' => 'practitioner',
            'email' => 'practitioner@online.fr',
            'password' => bcrypt('secret'),
            'address' => '2, rue du 14 juillet',
            'zip' => '58000',
            'city' => 'Nevers',
            'radius' => 25,
            'latitude' => 46.9882,
            'longitude' => 3.15722,
            'siret' => '54206547900926',
            'certificate' => '',
            'IBAN' => 'FR7614410000011234567890163',
            'id_user' => $id
        ]);

    }
}
