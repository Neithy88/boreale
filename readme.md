# Boréale

Boréale est une plateforme de mise en relation entre des socio-esthéticien(ne)s et des patients.

## A savoir (pour les développeurs)

#### Accès à la base de données

Editer le fichier **.env** à la racine du dépôt.

#### Assets

Les fichiers **app.js** et **app.css** sont ignorés dans le dépôt et doivent être compilés avec la commande **npm run dev** ou **npm run watch** (pour être compilés automatiquement à chaque changement effectué).

#### Textes

La plupart des pages nécessitent que la table **texts** soit remplie dans la base de données puisqu'elles contiennent des textes éditables par les administrateurs du site :
Il faut également remplir, dans le Controller.php, le tableau $page de la fonction updateText(), avec le nom des views (en français) et le nom correspondant dans la base de données (en anglais), à l'exception de la page home.

Le fichier **texts.sql** se trouvant dans le dossier **other** à la racine du dépôt permet d'importer les textes nécessaires.

#### Emails

Le contenu des emails envoyés par le site est également stocké dans la base de données et peut être édité dans l'espace administrateur.

Le fichier **mails.sql** se trouvant dans le dossier **other** à la racine du dépôt permet d'importer les mails par défaut.

#### Conventions utilisées

Les conventions suivantes sont utilisées dans ce dépôt :

* Indentation avec des tabulations et non des espaces
* Nom de variables et commentaires en anglais
* Tirets utilisés pour les noms de classes et d'ids en HTML/CSS
* Camelcase pour les noms de variables et de fonctions en JS
* Camelcase pour les noms de fonctions en PHP, underscores pour les noms de variables
* Pas d'espaces flottants
* Un saut de ligne en fin de fichier


## To know (for developpers)

#### Database access

Edit the **.env** file in the root directory of the repository.

#### Assets

The **app.js** and **app.css** files are ignored in the repository and have to be compiled with the **npm run dev** or **npm run watch** commands (for compilation after each change).

#### Texts

Most of the pages require the table **texts** to be filled in the database because the texts are editable by the website administrators :
The array $page (Controller.php, function updateText) has to be filled, for each editable page, with views names (in french) and corresponding names in database (in english), except for the home page.

The **texts.sql** file in the **other** directory in the root of the repository allows to import the necessary texts.

#### Emails

The contents of the emails sent by the website are stored in the database as well and can de edited in the administrator area.

The **mails.sql** file in the **other** directory in the root of the repository allows to import the default texts of emails.

#### Conventions utilisées

The following conventions are followed for this repository :

* Indentation with tabs and not spaces
* Variable names and comments in english
* Dashes for class and id names in HTML/CSS
* Camelcase for variable and function names in JS
* Camelcase for function names in PHP, underscores for variables
* No trailing spaces
* One new line at the end of file
