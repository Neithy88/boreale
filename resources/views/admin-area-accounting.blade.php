@extends('default')

@section('title')
	Espace administrateur
@endsection

@section('content')
<main id="admin-compta">
    <div id="user-header">
        <p>Bonjour, <span>{{ session('fname') }}</span></p>
    </div>
</main>
@endsection

@section('scripts')
@endsection
