@extends('default')

@section('title')
	Qui sommes-nous?
@endsection

@section('content')

<main id="about-us">
	<header class="page-header">
	</header>
	<q class="editable">{{ $texts[0] }}</q>
	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<section>
		<header class="mid-page-header">
				<h3 class="editable">{{ $texts[1] }}</h3>
				@if (session('type') == 'admin')
					<div id="edit-group1" class="hidden edit-area">
						<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
						<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
						<button type="button" id="edit-valid1" class="edit-valid">valider</button>
					</div>
					<div id="edit-icon1" class="edit-icon"></div>
				@endif
		</header>

		<article>
			<h4 class="editable">{{ $texts[2] }}</h4>
			@if (session('type') == 'admin')
				<div id="edit-group2" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area2" data-title="2">{{ $texts[2] }}</textarea>
					<button type="button" id='edit-cancel2' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid2" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon2" class="edit-icon"></div>
			@endif
			<p class="editable">{{ $texts[3] }}</p>
			@if (session('type') == 'admin')
				<div id="edit-group3" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area3" data-title="3">{{ $texts[3] }}</textarea>
					<button type="button" id='edit-cancel3' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid3" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon3" class="edit-icon"></div>
			@endif
		</article>

		<article>
			<h4 class="editable">{{ $texts[4] }}</h4>
			@if (session('type') == 'admin')
				<div id="edit-group4" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area4" data-title="4">{{ $texts[4] }}</textarea>
					<button type="button" id='edit-cancel4' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid4" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon4" class="edit-icon"></div>
			@endif
			<p class="editable">{{ $texts[5] }}</p>
			@if (session('type') == 'admin')
				<div id="edit-group5" class="hidden edit-area">
					<textarea class="edit-textarea"  name="edit-area5" data-title="5">{{ $texts[5] }}</textarea>
					<button type="button" id='edit-cancel5' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid5" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon5" class="edit-icon"></div>
			@endif
		</article>

		<article>
			<h4 class="editable">{{ $texts[6] }}</h4>
			@if (session('type') == 'admin')
				<div id="edit-group6" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area6" data-title="6">{{ $texts[6] }}</textarea>
					<button type="button" id='edit-cancel6' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid6" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon6" class="edit-icon"></div>
			@endif
			<p class="editable">{{ $texts[7] }}</p>
			@if (session('type') == 'admin')
				<div id="edit-group7" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area7" data-title="7">{{ $texts[7] }}</textarea>
					<button type="button" id='edit-cancel7' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid7" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon7" class="edit-icon"></div>
			@endif
		</article>

	</section>
</main>

@endsection

@section('scripts')
@endsection
