@extends('default')

@section('title')
	Facturations
@endsection

@section('content')
<main id="practitioner-accounting">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span></p>
	</div>

	<q class="editable">{{ $texts[0] }}</q>
    @if (session('type') == 'admin')
			<div id="edit-group0" class="hidden edit-area">
				<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
				<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
				<button type="button" id="edit-valid0" class="edit-valid">valider</button>
			</div>
			<div id="edit-icon0" class="edit-icon"></div>
		@endif

		<h4>Mes factures</h4>
</main>
@endsection
