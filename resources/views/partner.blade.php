@extends('default')

@section('title')
	Nos partenaires
@endsection

@section('content')

<main id="partner">
	<q class="editable">{{ $texts[0] }}</q>

	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<section>
		<header class="mid-page-header">
				<h3 class="editable">{{ $texts[1] }}</h3>
				@if (session('type') == 'admin')
					<div id="edit-group1" class="hidden edit-area">
						<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
						<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
						<button type="button" id="edit-valid1" class="edit-valid">valider</button>
					</div>
					<div id="edit-icon1" class="edit-icon"></div>
				@endif
		</header>


		<article>
			<div class="partners">
				<div class="partner-informations">
					<div class="overlay-informations">
						<div id="icon-close"></div>
						<h4>{{ $texts[2] }}</h4>
						<p>{{ $texts[3] }}</p>
					</div>
					<img src="{{ asset('/img/test-logo/test1.png') }}">
				</div>
				<div class="partner-informations">
					<img src="{{ asset('/img/test-logo/test2.png') }}">
				</div>
				<div class="partner-informations">
					<img src="{{ asset('/img/test-logo/test3.png') }}">
				</div>
			</div>
		</article>

	</section>
</main>

@endsection

@section('scripts')
@endsection
