@extends('default')

@section('title')
Espace profil
@endsection

@section('content')

<main id="profile">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span></p>
	</div>

	<q class="editable">{{ $texts[0] }}</q>

	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<section>
		<header class="mid-page-header">
				<h3 class="editable">{{ $texts[1] }}</h3>

				@if (session('type') == 'admin')
					<div id="edit-group1" class="hidden edit-area">
						<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
						<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
						<button type="button" id="edit-valid1" class="edit-valid">valider</button>
					</div>
					<div id="edit-icon1" class="edit-icon"></div>
				@endif
		</header>

		<form id="profile-form" method="post" action="{{ url('update') }}">
			@csrf

			<span class="confirmations" id="confirmation-profile"></span>

			<label for="name">{{__('Votre nom')}}</label>
			<input id="name" type="text" name="name" value="{{ $name }}">
			<span class="errors" id="error-name"></span>

			<label for="fname">{{__('Votre prénom')}}</label>
			<input id="fname" type="text" name="fname" value="{{ $fname }}">
			<span class="errors" id="error-fname"></span>

			<label for="email">{{__('Votre adresse mail')}}</label>
			<input id="email" type="email" name="email" value="{{ $email }}" data-email="{{session('user')}}">
			<span class="errors" id="error-mail"></span>

			@if(session('type') !== 'admin')
			<label for="address">{{__('Votre adresse')}}</label>
			<input id="address" type="text" name="address" value="{{ $address }}">
			<span class="errors" id="error-address"></span>

			<label for="zip">{{__('Votre code postal')}}</label>
			<input id="zip" type="text" name="zip" value="{{ $zip }}">
			<span class="errors" id="error-zip"></span>

			<label for="city">{{__('Votre commune')}}</label>
			<input id="city" type="text" name="city" value="{{ $city }}">
			<span class="errors" id="error-city"></span>

			@if(session('type') === 'practitioner')

			<p><span>Attention :</span> les informations suivantes sont nécessaires pour que les patients soient en mesure de vous trouver, et prendre rendez-vous.</p>

			<label for="radius">{{__('Votre rayon d\'action')}}</label>
			<input id="radius" type="text" name="radius" value="@if ($radius !== 0){{ $radius }} @endif">
			<span class="errors" id="error-radius"></span>

			<label for="siret">{{__('Votre numéro de siret')}}</label>
			<input id="siret" type="text" name="siret" value="@if ($siret !== 0){{ $siret }} @endif">
			<span class="errors" id="error-siret"></span>

			<label for="iban">{{__('Votre IBAN')}}</label>
			<input id="iban" type="text" name="iban" value="@if ($iban !== 0){{ $iban }} @endif">
			<span class="errors" id="error-iban"></span>

<!-- TO DO LABEL FOR DOWNLOAD THE CERTIFICATE

			<label for="file">__('Votre photo de diplôme')</label>
			<input id="file" type="file" name="file" value="">
			<span id="error-file"></span>
-->
			@endif
			@endif
			<input type="submit" name="submit-btn">
		</form>

		<form method="post" action="{{ url('changePsw')}}">
			@csrf

			<input type="password" id="password" name="password" placeholder="Mot de passe">
			<span class="errors" id="error-pw"></span>

			<input type="password" name="new-password" placeholder="Nouveau mot de passe">
			<span class="errors" id="error-npw"></span>

			<input type="password" name="confirm-password" placeholder="Confirmer mot de passe">
			<span class="errors" id="error-cpw"></span>

			<input type="submit" name="submit-btn">
		</form>
	</section>
</main>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/profile.js') }}"></script>
@endsection
