<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;

class PractitionerController extends Controller {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function practitionerArea() {
		if (session('type') === 'practitioner' || session('type') === 'admin') {
			$texts = getTexts('practitioner-area');
			return view('practitioner-area', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function practitionerAreaAccounting() {
		if (session('type') === 'practitioner' || session('type') === 'admin') {
			$texts = getTexts('practitioner-accounting');
			return view('practitioner-accounting', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function practitionerSelectServices() {
		$services = Input::get('service');
		$mail = session('user');
		$id_practitioner = idPractitioner($mail)->id_practitioner;

		// Delete all services from db
		deselectServices($id_practitioner);

		// Insert checked services into db
		if (!empty($services)) {
			foreach ($services as $service => $id_service) {
				selectService($id_service, $id_practitioner);
			}
		}

		return redirect()->to('/espace-praticien/prestations')->send();
	}

	public function getAppointsPractitioner() {

		if (session('type') === 'practitioner') {

			$idu = getIdByMail(session('user')); // get id
			$id = getId($idu);
			$appoints = getAppointsPat($id); //get all appointments for this id
			return json_encode($appoints);
		}
	}

}
