<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;

class PatientController extends Controller {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function patientArea() {
		if (session('type') === 'patient' || session('type') === 'admin') {
			$texts = getTexts('patient-area');
			return view('patient-area', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function agendaPatientMakeAppoint($id) {

		$id = filter_var($id, FILTER_SANITIZE_STRING);
		if (!getId($id, 'practitioner')) // test if id is valid
			abort(404);

		$texts = getTexts('agenda');
		return view('agenda', ['texts' => $texts, 'id' => $id]);
	}

	public function servicesSearch() {
		if(session('type') === 'patient' || session('type') === 'admin'){
			$texts = getTexts('prestations');
			return view('search', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function getAppointsPatient() {

		if (session('type') === 'patient') {

			$idu = getIdByMail(session('user')); // get id user
			$id = getId($idu); // get id by id user
			$appoints = getAppointsPat($id); //get all appointments for this id
			return json_encode($appoints);
		}
		else
			return false;
	}

	public function getUnavailability() {

		if (session('type') === 'patient') {

			$idu = getIdByMail(session('user')); // id user of patient
			$idPat = getId($idu); // id patient
			$idPract = Input::get('data-id-pract');
			$unavailablesAppoints = getUnavailablesPract($idPract, $idPat); //get all appointments for this id
			$unavailablesHours = arrayByHours($unavailablesAppoints); // isolate each unavailable hour (separate hours of same appointment)
			return json_encode($unavailablesHours);
		}
		else
			return false;
	}

	public function searchAddress() {
		$mail = session('user');
		$address = getPatientAddress($mail);

		return json_encode($address);
	}

	public function searchLoc() {
		$mail = session('user');
		$loc = getPatientLoc($mail);

		return json_encode($loc);
	}

	public function searchPractitioners() {
		$latitude = Input::get('lat');
		$longitude = Input::get('lon');

		$practitioners = getPractitionersLoc();

		$valid_practitioners = [];

		foreach ($practitioners as $practitioner) {
			$lat = $practitioner->latitude;
			$lon = $practitioner->longitude;
			$rad = $practitioner->radius;

			// if coordinates are defined calculate distance between
			// patient and practitioner
			if ($lat != 0 && $lon != 0) {

				$lat_from = deg2rad($latitude);
				$lon_from = deg2rad($longitude);
				$lat_to = deg2rad($lat);
				$lon_to = deg2rad($lon);

				$lat_delta = $lat_to - $lat_from;
				$lon_delta = $lon_to - $lon_from;

				$angle = 2 * asin(sqrt(pow(sin($lat_delta / 2), 2) +
					cos($lat_from) * cos($lat_to) * pow(sin($lon_delta / 2), 2)));

				$earth_radius = 6371; // km

				$dist = $angle * $earth_radius;

				$practitioner->distance = $dist;

				// Reset lat and lon to prevent the user from accessing them
				unset($practitioner->latitude);
				unset($practitioner->longitude);

				// If distance is inferior or equal to radius of action
				if ($dist <= $rad) {
					$id = $practitioner->id_practitioner;
					// Get services list for the practitioner
					$services = getPractitionerServices($id);
					$practitioner->services = $services;

					// Add the practitioner to those returned with Ajax
					array_push($valid_practitioners, $practitioner);
				}
			}
		}

		return json_encode($valid_practitioners);
	}

}
